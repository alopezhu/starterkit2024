from PyConf.reading import get_particles, get_pvs

from DaVinci import make_config, Options
from DaVinci.algorithms import create_lines_filter
from FunTuple import FunTuple_Particles as Funtuple
from GaudiKernel.SystemOfUnits import MeV
from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC
import Functors as F
from DaVinciMCTools import MCTruthAndBkgCat


def main(options: Options):
    line = "Hlt2Charm_DstpToD0Pip_D0ToKmPip"
    data = get_particles(f"/Event/HLT2/{line}/Particles")
    line_prefilter = create_lines_filter(name=f"PreFilter_{line}", lines=[line])


    pvs = get_pvs()

    fields = {
        "Dstar" : "[D*(2010)+ -> (D0 -> K- pi+) pi+]CC",
        "D0"    : "[D*(2010)+ -> ^(D0 -> K- pi+) pi+]CC",
        "Kminus": "[D*(2010)+ -> (D0 -> ^K- pi+) pi+]CC",
        "piplus": "[D*(2010)+ -> (D0 -> K- ^pi+) pi+]CC",
        "pisoft": "[D*(2010)+ -> (D0 -> K- pi+) ^pi+]CC",
    }

    variables = FC.Kinematics()
    variables_head = FunctorCollection({})

    variables += FunctorCollection({
        "ETA"      : F.ETA,
        "PHI"      : F.PHI,
        "CHI2"     : F.CHI2,
        "CHI2DOF"  : F.CHI2DOF,
        "BPVX"     : F.BPVX(pvs),
        "BPVY"     : F.BPVY(pvs),
        "BPVZ"     : F.BPVZ(pvs),
        "BPVIP"    : F.BPVIP(pvs),
        "BPVIPCHI2": F.BPVIPCHI2(pvs),
        "CHARGE"   : F.CHARGE,
        "ISBASIC"  : F.ISBASICPARTICLE,
        "HASTRACK" : F.HAS_VALUE @ F.TRACK,
    })

    variables_comp = FunctorCollection({
        "BPVFDCHI2": F.BPVFDCHI2(pvs),
        "BPVFD": F.BPVFD(pvs),
        "BPVLTIME": F.BPVLTIME(pvs),
        "BPVDIRA": F.BPVDIRA(pvs),
        "MAXDOCACHI2": F.MAXDOCACHI2,
        "MAXDOCA": F.MAXDOCA,
        "MAXSDOCACHI2": F.MAXSDOCACHI2,
        "MAXSDOCA": F.MAXSDOCA,
        "END_VX": F.END_VX,
        "END_VY": F.END_VY,
        "END_VZ": F.END_VZ,
        "MAX_PT"   : F.MAX(F.PT),
        "MIN_PT"   : F.MIN(F.PT),
        "SUM_PT"   : F.SUM(F.PT),
        "MAX_P"    : F.MAX(F.P),
        "MIN_P"    : F.MIN(F.P),
        "PARTICLE_ID": F.PARTICLE_ID,
        "max_pt"   : F.MAXTREE(F.ISBASICPARTICLE & (F.HAS_VALUE @ F.TRACK), F.PT),
        'n_highpt_tracks': F.NINTREE(F.ISBASICPARTICLE & (F.HAS_VALUE @ F.TRACK) & (F.PT > 1500*MeV)),
    })

    variables_tracks = FC.ParticleID(extra_info=True)

    variables_tracks += FunctorCollection({
        "GHOSTPROB": F.GHOSTPROB,
        "NHITS": F.VALUE_OR(-1) @ F.NHITS @ F.TRACK,
        "NVPHITS": F.VALUE_OR(-1) @ F.NVPHITS @ F.TRACK,
        "NFTHITS": F.VALUE_OR(-1) @ F.NFTHITS @ F.TRACK,
        "QOVERP": F.QOVERP @ F.TRACK,
        "TRACK_P": F.TRACK_MOMVEC,
        "TrP": F.TRACK_P,
    })

    if options.simulation == True:
        mctruth = MCTruthAndBkgCat(input_particles=data, name="MCTruthAndBkgCat_functor")

        variables += FC.MCKinematics(mctruth_alg=mctruth)
        variables += FC.MCHierarchy(mctruth_alg=mctruth)
        variables += FC.MCPrimaryVertexInfo(mctruth_alg=mctruth)
        variables_head.update({"BKGCAT": mctruth.BkgCat})
        variables_head += FC.MCPromptDecay(mctruth_alg=mctruth)
        variables_comp += FC.MCVertexInfo(mctruth_alg=mctruth)
        
    Hlt1_decisions = ["Hlt1TrackMVA", "Hlt1TwoTrackMVA"]
    variables+=FC.HltTisTos(
        selection_type="Hlt1",
        trigger_lines=[f"{x}Decision" for x in Hlt1_decisions],
        data=data)

    branches = {
        "ALL"   : variables,
        "Dstar" : variables_comp+variables_head,
        "D0"    : variables_comp,
        "piplus" : variables_tracks,
        "Kminus": variables_tracks,
        "pisoft": variables_tracks
        }

    event_info = FC.EventInfo()
    event_info += FC.SelectionInfo(selection_type="Hlt1", trigger_lines=Hlt1_decisions)

    funtuple = Funtuple(
        name="myTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=branches,
        inputs=data,
        event_variables=event_info,
    )

    algs = {line: [line_prefilter, funtuple]}

    return make_config(options, algs)
