from PyConf.reading import get_particles, get_pvs

from DaVinci import make_config, Options
from DaVinci.algorithms import create_lines_filter
from FunTuple import FunTuple_Particles as Funtuple
from FunTuple import FunctorCollection
import FunTuple.functorcollections as FC
import Functors as F


def main(options: Options):
    line = "Hlt2Charm_DstpToD0Pip_D0ToKmKp"
    data = get_particles(f"/Event/HLT2/{line}/Particles")
    line_prefilter = create_lines_filter(name=f"PreFilter_{line}", lines=[line])

    pvs = get_pvs()

    fields = {
        "Dst": "D*(2010)+ -> (D0 -> K+ K-) pi+",
        "D0" : "D*(2010)+ -> ^(D0 -> K+ K-) pi+",
        "pisoft": "D*(2010)+ -> (D0 -> K+ K-) ^pi+",
        "Kp" : "D*(2010)+ -> (D0 -> ^K+ K-) pi+",
        "Km" : "D*(2010)+ -> (D0 -> K+ ^K-) pi+"
    }

    variables = FunctorCollection({
        "M": F.MASS,
        "P": F.P,
        "PT": F.PT})

    variables = {"ALL": variables}


    funtuple = Funtuple(
        name="myTuple",
        tuple_name="DecayTree",
        fields=fields,
        variables=branches,
        inputs=data,
    )

    algs = {line: [line_prefilter, funtuple]}

    return make_config(options, algs)
